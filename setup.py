import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="plotting_utils",
    version="0.0.12",
    author="Edouard Leroy",
    author_email="edoleroy@gmail.com",
    description="A package for nice plots in matplotlib and other utilities",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/edoleroy/plotting_utilities",
    packages=setuptools.find_packages(),
	install_requires=['numpy>=1.16','matplotlib>=3'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)

from distutils.core import run_setup
run_setup("setup.py",["sdist", "bdist_wheel"])
run_setup("setup.py",["rotate", "--match=.tar.gz,.whl", "--keep=3"])
run_setup("setup.py",["clean", "--all"])
run_setup("setup.py",["install"])

import shutil, glob; 
egg_folder = glob.glob('*.egg-info')
if len(egg_folder)>0:
    shutil.rmtree(egg_folder[0])

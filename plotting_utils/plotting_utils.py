# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 07:37:14 2019

@author: Edouard
"""

import matplotlib.pyplot as pp
import matplotlib
import numpy as np

AXIS_COLOR = "#000000"
GRID_COLOR = AXIS_COLOR
#GRID_COLOR = '#909090'
PLOT_COLORS = ["#ED553B", "#47AB6C", "#112F41", "#0894A1", "#F2B134"]
matplotlib.rcParams['font.sans-serif'] = "Helvetica"
matplotlib.rcParams['font.size'] = 14
matplotlib.rcParams['axes.grid'] = True
matplotlib.rcParams['grid.color'] = GRID_COLOR
matplotlib.rcParams['grid.linestyle'] = ':'
matplotlib.rcParams['grid.linewidth'] = 0.7
matplotlib.rcParams['axes.edgecolor'] = AXIS_COLOR
matplotlib.rcParams['axes.linewidth'] = 1.2


matplotlib.rcParams['text.color'] = AXIS_COLOR
matplotlib.rcParams['axes.labelcolor'] = AXIS_COLOR
matplotlib.rcParams['xtick.color'] = AXIS_COLOR
matplotlib.rcParams['xtick.major.width'] = 1.2
matplotlib.rcParams['legend.fontsize']= 14
matplotlib.rcParams['legend.title_fontsize'] = 14
matplotlib.rcParams["legend.borderpad"] = 0.2
matplotlib.rcParams['legend.labelspacing']=0.1 

matplotlib.rcParams["legend.edgecolor"] = "inherit"
matplotlib.rcParams['ytick.color'] = AXIS_COLOR
matplotlib.rcParams['ytick.major.width'] = 1.2
matplotlib.rcParams['figure.dpi'] = 80
matplotlib.rcParams['savefig.dpi'] = 300


def convert_to_iterable(obj):
    try:
        iter(obj)
    except TypeError:
        obj = [obj]
    return obj
NICE_PLOT_DEFAUT_PARAMETERS = dict({"hw_ratio": 0.5,
                 "xLim": None,
                 "leg_on": False,
                 "ax": pp.gca(),
                 "side": "left",
                 "line_colors": PLOT_COLORS,
                 "line_style": [""],
                 "marker_style": ["s", "o"],
                 "line_width": 1,
                 "legend_title": None,
                 "force_ymin": 0,
                 "legend_position": "best",
                 "anchor_legend": None,
                 "nyticks": "auto",
                 "yaxis_color": None
                 })
def nice_plot(**kwargs):
    """
    hw_ratio=0.5, xLim=None,leg_on=False,ax=None,side="left", 
    line_colors = PLOT_COLORS, line_style = [""], marker_style  = ["s", "o"], 
    line_width = 1, legend_title = None, force_ymin = 0, legend_position="best", nyticks = "auto",
    yaxis_color = None
    """
    parameters = NICE_PLOT_DEFAUT_PARAMETERS.copy()
    parameters["ax"] = pp.gca()
    parameters.update(kwargs)   
    
    ax = parameters["ax"] #Current axis
    
    #Line style
    markers = convert_to_iterable(parameters["marker_style"])
    ls = convert_to_iterable(parameters["line_style"])
    lc = convert_to_iterable(parameters["line_colors"])    
    lw = convert_to_iterable(parameters["line_width"])
  
    for i,  line in enumerate(ax.lines):
        line.set_marker(markers[i % len(markers)])
        line.set_markersize(4)
        line.set_linewidth(lw[i%len(lw)])
        line.set_linestyle(ls[i%len(ls)])
        line.set_color(lc[i%len(lc)])
        line.set_clip_on(False)
        
    #Axis shifting
    ax.spines['top'].set_visible(False)   
    if parameters["side"]=="left":
        ax.spines['right'].set_visible(False)
        ax.spines["left"].set_position(("axes",-0.03)) # Offset the left scale from the axis
    else:
        ax.spines['left'].set_visible(False)
        ax.spines["right"].set_position(("outward",7)) # Offset the left scale from the axis
        # ax.spines["right"].set_smart_bounds(True)
    ax.spines["bottom"].set_position(("axes",-0.05)) # Offset the bottom scale from the axis
    
    if parameters["yaxis_color"]:
        ax.spines[parameters["side"]].set_color(parameters["yaxis_color"])
        ax.yaxis.label.set_color(parameters["yaxis_color"])
        ax.tick_params(axis='y', colors=parameters["yaxis_color"])

    ##Axis scale and ticks
    ax.autoscale(enable=True, axis='both', tight=True)      
    xLim = parameters["xLim"]
    force_ymin = parameters["force_ymin"]
    
    if(xLim):
        ax.set_xlim([xLim[0],xLim[1]])
    xticks=ax.get_xticks()
    xmax=ax.get_xlim()[1]
    
    if(xmax>xticks[-1]):
        xticks=np.append(xticks,xticks[-1]+xticks[1]-xticks[0])
    ax.set_xticks(xticks)
    
    ymax = ax.get_ylim()[1]
    if force_ymin: ax.set_ylim([force_ymin, ymax])
    yticks = ax.get_yticks()
    ymax = ax.get_ylim()[1]

    if(ymax>yticks[-1]):
        yticks=np.append(yticks,yticks[-1]+yticks[1]-yticks[0])
    if(parameters["nyticks"]!="auto"):
        yticks = np.linspace(yticks.min(), yticks.max(), parameters["nyticks"])
    ax.set_yticks(yticks)
    ax.set_ylim(yticks.min(), yticks.max())
    

    #Legend    
    if parameters["leg_on"]:
        pp.legend(title = parameters["legend_title"], 
                    loc=parameters["legend_position"], 
                    ncol=1,  
                    fancybox=False, 
                    framealpha=1,
                    bbox_to_anchor=parameters["anchor_legend"])
        
    #Set figure size
    fig = pp.gcf()
    width, height = fig.get_size_inches()
    fig.set_size_inches(width, width*parameters["hw_ratio"])
    
def plot_two_parameters_scatter(param2_array, xdata, ydata, error_bars=None, labels = [None, None], legend_label = "%.2f", save_path = None):
    pp.figure(7)
    pp.clf()
    leg = []
    for param_2 in np.unique(param2_array):
        leg.append(legend_label % param_2)
        sel_data = param2_array==param_2
        if error_bars is not None:
            pp.errorbar(xdata[sel_data], ydata[sel_data], yerr = error_bars[sel_data], fmt='o', label=legend_label % param_2)
        else:
            pp.scatter(xdata[sel_data], ydata[sel_data], label=legend_label % param_2)
        if labels[0] is not None:
            pp.xlabel(labels[0])
        if labels[1] is not None:
            pp.ylabel(labels[1])        
#    pp.legend(leg)
    nice_plot(leg_on=True, xLim = [0, xdata.max()])
    pp.savefig(save_path, dpi=300, bbox_inches="tight")

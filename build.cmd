python setup.py sdist bdist_wheel
python setup.py rotate --match=.tar.gz,.whl --keep=3
python setup.py clean --all
python -c "import shutil, glob; shutil.rmtree(glob.glob('*.egg-info')[0])"
